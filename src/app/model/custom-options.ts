import {ToastOptions} from 'ngx-toasta';


export class CustomOption extends ToastOptions {
  title: string;
  msg: string;
  timeout: any;
  showClose: any;
  theme: any;
  position: any;
  limit: any;

  public constructor(title: string, msg: string ) {
    super();
    this.title = title;
    this.msg = msg;
    this.timeout = 5000;
    this.showClose = true;
    this.theme = 'bootstrap';
    this.position = 'top-center';
    this.limit = 5;
  }
}




