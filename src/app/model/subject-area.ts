import {Journal} from './journal';
import {Editor} from './editor';

export class SubjectArea {
  id: number;
  area: string;
  areaEditor: Editor;
  journal: Journal;

  public constructor() {
    this.id = 0;
    this.area = '';
    this.areaEditor = new Editor();
    this.journal = new Journal();
  }
}
