import {ScientificWorks} from './scientific-works';

export class TaskDto {
  taskId: string;
  processInstanceId: string;
  data: ScientificWorks;

  public constructor() {
    this.taskId = '';
    this.processInstanceId = '';
    this.data = new ScientificWorks();
  }

}
