export class SearchResult {
  title: string;
  journal: string;
  subjectArea: string;
  doi: string;
  keywords: string;
  location: string;
  openAccess: boolean;
  authors: string;
  highlight: string;

  public constructor() {
    this.title = '';
    this.journal = '';
    this.subjectArea = '';
    this.doi = '';
    this.keywords = '';
    this.location = '';
    this.openAccess = true;
    this.authors = '';
    this.highlight = '';
  }
}
