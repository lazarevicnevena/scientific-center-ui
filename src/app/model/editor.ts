import {Journal} from './journal';

export class Editor {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  city: string;
  state: string;
  rank: string;
  journal: Journal;

  public constructor() {
    this.id = 0;
    this.firstName = '';
    this.lastName = '';
    this.email = '';
    this.city = '';
    this.state = '';
    this.rank = '';
    this.journal = new Journal();
  }
}
