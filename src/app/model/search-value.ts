export class SearchValue {
  field: string;
  value: string;
  operator: string;
  type: string;

  public constructor() {
    this.field = 'title';
    this.value = '';
    this.operator = 'AND';
    this.type = 'regular';
  }
}
