export class FormDetails {
  taskId: string;
  formFields: any;
  processInstanceId: string;

  public constructor() {
    this.taskId = '';
    this.formFields = [];
    this.processInstanceId = '';
  }
}
