export class Author {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  city: string;
  state: string;

  public constructor() {
    this.id = 0;
    this.firstName = '';
    this.lastName = '';
    this.email = '';
    this.city = '';
    this.state = '';
  }
}
