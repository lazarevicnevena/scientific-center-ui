export class CurrentUser {
  username: string;
  role: string;

  public constructor(username: string) {
    this.username = username;
    this.role = '';
  }
}
