import {Author} from './author';
import {Journal} from './journal';

export class ScientificWorks {
  id: number;
  title: string;
  subjectArea: string;
  keywords: string;
  shortAbstract: string;
  authors: Author[];
  journalTitle: string;
  location: string;
  status: string;

  public constructor() {
    this.id = 0;
    this.title = '';
    this.subjectArea = '';
    this.keywords = '';
    this.shortAbstract = '';
    this.authors = [];
    this.journalTitle = '';
    this.location = '';
    this.status = '';
  }
}
