import {ScientificWorks} from './scientific-works';

export class MinorChangesDetails {
  id: number;
  answeringComments: string;
  lastName: string;
  scientificWork: ScientificWorks;

  public constructor() {
    this.id = 0;
    this.answeringComments = '';
    this.scientificWork = new ScientificWorks();
  }
}
