import {Editor} from './editor';
import {Reviewer} from './reviewer';
import {SubjectArea} from './subject-area';

export class Journal {
  id: number;
  title: string;
  issn: string;
  openAccess: boolean;
  editor: Editor;
  subjectAreas: SubjectArea[];
  reviewers: Reviewer[];


  public constructor() {
    this.id = 0;
    this.title = '';
    this.issn = '';
    this.openAccess = true;
    this.editor = new Editor();
    this.subjectAreas = [];
    this.reviewers = [];
  }
}
