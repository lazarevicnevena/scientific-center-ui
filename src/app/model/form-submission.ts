export class FormSubmission {
  fieldId: string;
  fieldValue: string;

  public constructor(fieldId: string, fieldValue: string) {
    this.fieldId = fieldId;
    this.fieldValue = fieldValue;
  }
}
