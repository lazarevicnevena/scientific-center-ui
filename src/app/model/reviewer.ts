export class Reviewer {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  city: string;
  state: string;
  rank: string;

  public constructor() {
    this.username = '';
    this.firstName = '';
    this.lastName = '';
    this.email = '';
    this.city = '';
    this.state = '';
    this.rank = '';
  }
}
