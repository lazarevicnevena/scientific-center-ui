import {Reviewer} from './reviewer';

export class ReviewDetails {
  id: number;
  commentToAuthor: string;
  recommendation: string;
  commentToEditor: string;
  status: string;
  reviewer: Reviewer;

  public constructor() {
    this.id = 0;
    this.commentToAuthor = '';
    this.recommendation = '';
    this.commentToEditor = '';
    this.status = '';
    this.reviewer = new Reviewer();
  }
}
