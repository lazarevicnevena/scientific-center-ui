import {Component, ViewContainerRef} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from './service/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'scientific-center-ui';

  constructor(private router: Router,
              private loginService: LoginService) {
  }

  logut(): void {
    localStorage.removeItem('currentUser');
    this.router.navigateByUrl('/');
  }

  loggedIn(): boolean {
    if (localStorage.getItem('currentUser') != null) {
      return true;
    }
    return false;
  }

  getRole(): string {
    return this.loginService.getCurrentUserRole();
  }
}
