import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormDetails} from '../model/form-details';
import {FormSubmission} from '../model/form-submission';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }

  public headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  register(taskId: string, formSubmission: FormSubmission[]): Promise<FormDetails> {
    return this.http.post(`/api/users/tasks/${taskId}`, formSubmission, { headers: this.headers })
      .toPromise()
      .then(res => res as FormDetails);
  }

}
