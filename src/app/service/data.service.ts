import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {FormDetails} from '../model/form-details';

@Injectable()
export class DataService {

  private messageSource = new BehaviorSubject(new FormDetails());
  currentFormDetails = this.messageSource.asObservable();

  constructor() { }

  changeFormDetails(formDetails: FormDetails) {
    this.messageSource.next(formDetails);
  }
}
