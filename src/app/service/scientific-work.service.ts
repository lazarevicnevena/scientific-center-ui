import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {FormDetails} from '../model/form-details';
import {FormSubmission} from '../model/form-submission';
import {TaskDto} from '../model/task';
import {ResponseWrapper} from '../model/response-wrapper';
import {ScientificWorks} from '../model/scientific-works';
import {SearchResult} from '../model/search-result';
import {ReviewDetails} from '../model/review-details';
import {MinorChangesDetails} from '../model/minor-changes-details';
import {SearchValue} from '../model/search-value';

@Injectable({
  providedIn: 'root'
})
export class ScientificWorkService {

  constructor(private http: HttpClient) { }


  private worksUrl = '/api/scientific-works';

  submitScientificWork(taskId: string, formData: FormData): Promise<FormDetails> {

    return this.http.post(this.worksUrl + `/tasks/${taskId}`, formData)
      .toPromise()
      .then(res => res as FormDetails);
  }

  submitAuthorsChangesFromRevision(taskId: string, formData: FormData): Promise<string> {

    return this.http.post(this.worksUrl + `/tasks/${taskId}/author-changes`, formData, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  getSubmitScientificWork(editor: string): Promise<TaskDto[]> {
    return this.http.get(this.worksUrl + `/submitted/editors/${editor}`)
      .toPromise()
      .then(res => res as TaskDto[]);
  }

  getMinorMajorChangedWorks(editor: string): Promise<TaskDto[]> {
    return this.http.get(this.worksUrl + `/editors/${editor}/minor-major-changed`)
      .toPromise()
      .then(res => res as TaskDto[]);
  }

  getScientificWorkById(id: number, author: string): Promise<ScientificWorks> {
    return this.http.get(this.worksUrl + `/${id}/authors/${author}`)
      .toPromise()
      .then(res => res as ScientificWorks);
  }

  getFormatInvalidComment(id: number): Promise<string> {
    return this.http.get(this.worksUrl + `/${id}/format-invalid-comment`, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  getRevisionComments(id: number): Promise<string[]> {
    return this.http.get(this.worksUrl + `/${id}/revision-comments`)
      .toPromise()
      .then(res => res as string[]);
  }

  getAcceptedWorks(pageNum: number, pageSize: number): Promise<ResponseWrapper> {

    let params = new HttpParams();
    params = params.append('pageNum', pageNum.toString());
    params = params.append('pageSize', pageSize.toString());

    const url = this.worksUrl + '/status/accepted';

    return this.http.get<ResponseWrapper>(url, {params: params, observe: 'response'})
      .toPromise()
      .then(works => works as ResponseWrapper);
  }

  search(search: SearchValue[]): Promise<SearchResult[]> {
    const url = this.worksUrl + '/searching';

    return this.http.post(url, search)
      .toPromise()
      .then(works => works as SearchResult[]);
  }

  chooseOptionForSubmittedWork(taskId: string, processInstanceId: string, formSubmissions: FormSubmission[]): Promise<string> {

    const url = this.worksUrl + `/validation/tasks/${taskId}/processes/${processInstanceId}`;
    return this.http.post(url, formSubmissions, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  getAssignedWorkForRevision(editor: string): Promise<TaskDto[]> {
    return this.http.get(this.worksUrl + `/editors/${editor}/to-revision`)
      .toPromise()
      .then(res => res as TaskDto[]);
  }

  getAuthorsWorksForChanging(author: string): Promise<TaskDto[]> {
    return this.http.get(this.worksUrl + `/authors/${author}/format-change`)
      .toPromise()
      .then(res => res as TaskDto[]);
  }

  getAuthorsWorksFromRevision(author: string): Promise<TaskDto[]> {
    return this.http.get(this.worksUrl + `/authors/${author}/from-revision`)
      .toPromise()
      .then(res => res as TaskDto[]);
  }

  getWorksForReviewing(reviewer: string): Promise<TaskDto[]> {
    return this.http.get(`/api/reviewers/${reviewer}/scientific-works/assigned`)
      .toPromise()
      .then(res => res as TaskDto[]);
  }

  getRevisionsForSW(id: number): Promise<ReviewDetails[]> {
    return this.http.get(this.worksUrl + `/${id}/revisions`)
      .toPromise()
      .then(res => res as ReviewDetails[]);
  }

  getReviewedScientificWork(editor: string): Promise<TaskDto[]> {
    return this.http.get(this.worksUrl + `/reviewed/editors/${editor}`)
      .toPromise()
      .then(res => res as TaskDto[]);
  }

  getAuthorChangesWrokFormDetails(author: string): Promise<FormDetails> {
    const url = `/api/reviewers/${author}/author-changes-work`;
    return this.http.get(url)
      .toPromise()
      .then(res => res as FormDetails);
  }

  getEditorSecondDecisionFormDetails(editor: string): Promise<FormDetails> {
    const url = this.worksUrl + `/editors/${editor}/second-decision`;
    return this.http.get(url)
      .toPromise()
      .then(res => res as FormDetails);
  }

  getMinorChangesDetails(id: number): Promise<MinorChangesDetails> {
    const url = this.worksUrl + `/${id}/minor-changes-details`;
    return this.http.get(url)
      .toPromise()
      .then(res => res as MinorChangesDetails);

  }
}
