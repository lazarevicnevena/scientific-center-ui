import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormDetails} from '../model/form-details';
import {FormSubmission} from '../model/form-submission';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  public headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  login(taskId: string, formSubmission: FormSubmission[]): Promise<FormDetails> {
    return this.http.post(`/api/login/tasks/${taskId}`, formSubmission, { headers: this.headers })
      .toPromise()
      .then(res => res as FormDetails);
  }

  getLoginOrRegistrationForm(registered: boolean): Promise<FormDetails> {
    return this.http.get(`/api/login/registered/${registered}`)
      .toPromise()
      .then(res => res as FormDetails);
  }

  getRole(username: string): Promise<string> {
    return this.http.get(`/api/users/${username}/role`, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  getCurrentUserUsername(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const username = currentUser && currentUser.username;
    return username ? username : '';
  }

  getCurrentUserRole(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const role = currentUser && currentUser.role;
    return role ? role : '';
  }

}
