import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {FormDetails} from '../model/form-details';
import {FormSubmission} from '../model/form-submission';
import {ResponseWrapper} from '../model/response-wrapper';
import {Reviewer} from '../model/reviewer';

@Injectable({
  providedIn: 'root'
})
export class ReviewerService {

  constructor(private http: HttpClient) {
  }


  private reviewersUrl = '/api/reviewers';

  getReviewers(processInstanceId: string, option: string, additional: boolean): Promise<ResponseWrapper> {

    const url = this.reviewersUrl + `/processes/${processInstanceId}/option/${option}/additional/${additional}`;
    return this.http.get(url, {observe: 'response'})
      .toPromise()
      .then(res => res as ResponseWrapper);
  }

  setReviewers(processInstanceId: string, taskId: string, reviewers: Reviewer[], days: number, option: string): Promise<string> {

    const url = this.reviewersUrl + `/choose/tasks/${taskId}/processes/${processInstanceId}/deadline/${days}/option/${option}`;

    return this.http.post(url, reviewers, {responseType: 'text'})
        .toPromise()
        .then(res => res as string);

  }

  additionalRevisionNeeded(processInstanceId: string, taskId: string): Promise<FormDetails> {
    const url = this.reviewersUrl + `/choose/tasks/${taskId}/processes/${processInstanceId}/additional`;

    return this.http.get(url)
      .toPromise()
      .then(res => res as FormDetails);
  }

  getRevisionFormDetails(reviewer: string): Promise<FormDetails> {
    const url = this.reviewersUrl + `/${reviewer}/revision-form-details`;
    return this.http.get(url)
      .toPromise()
      .then(res => res as FormDetails);
  }

  getEditorDecisionFormDetails(editor: string): Promise<FormDetails> {
    const url = this.reviewersUrl + `/${editor}/editor-decision-form-details`;
    return this.http.get(url)
      .toPromise()
      .then(res => res as FormDetails);
  }

  sendRevision(reviewer: string, taskId: string, processInstanceId: string, formSubmission: FormSubmission[]) {
    const url = this.reviewersUrl + `/${reviewer}/tasks/${taskId}/processes/${processInstanceId}/send-revision`;
    return this.http.post(url, formSubmission, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  decide(taskId: string, processInstanceId: string, formSubmission: FormSubmission[], option: string) {
    const url = this.reviewersUrl + `/tasks/${taskId}/processes/${processInstanceId}/decision/${option}`;
    return this.http.post(url, formSubmission, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }
}
