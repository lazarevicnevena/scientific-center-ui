import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ResponseWrapper} from '../model/response-wrapper';
import {FormSubmission} from '../model/form-submission';
import {FormDetails} from '../model/form-details';

@Injectable({
  providedIn: 'root'
})
export class JournalSevice {

  constructor(private http: HttpClient) {
  }

  private journalsUrl = '/api/journals';

  getJournals(pageNum: number, pageSize: number): Promise<ResponseWrapper> {

    let params = new HttpParams();
    params = params.append('pageNum', pageNum.toString());
    params = params.append('pageSize', pageSize.toString());

    const url = this.journalsUrl;

    return this.http.get<ResponseWrapper>(url, {params: params, observe: 'response'})
      .toPromise()
      .then(journals => journals as ResponseWrapper);
  }

  chooseJournal(taskId: string, formSubmission: FormSubmission[]): Promise<FormDetails> {
    const url = this.journalsUrl + `/tasks/${taskId}`;
    return this.http.post(url, formSubmission)
      .toPromise()
      .then(res => res as FormDetails);
  }


  registrationForJournal(taskId: string, formSubmission: FormSubmission[]): Promise<FormDetails> {
    const url = this.journalsUrl + `/users/tasks/${taskId}`;
    return this.http.post(url, formSubmission)
      .toPromise()
      .then(res => res as FormDetails);
  }

}
