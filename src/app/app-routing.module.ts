import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {ChoosingJournalComponent} from './components/journals/choosing-journal/choosing-journal.component';
import {JournalRegistrationComponent} from './components/journals/journal-registration/journal-registration.component';
import {SubmitScientificWorkComponent} from './components/scientific-works/submit-scientific-work/submit-scientific-work.component';
import {AuthorHomepageComponent} from './components/author/author-homepage/author-homepage.component';
import {EditorHomepageComponent} from './components/editor/editor-homepage/editor-homepage.component';
import {ReviewerHomepageComponent} from './components/reviewer/reviewer-homepage/reviewer-homepage.component';
import {SubmittedScientificWorksComponent} from './components/scientific-works/submitted-scientific-works/submitted-scientific-works.component';
import {ChoosingReviewersComponent} from './components/reviewer/choosing-reviewers/choosing-reviewers.component';
import {RevisionComponent} from './components/reviewer/revision/revision.component';
import {SubmittedRevisionsComponent} from './components/reviewer/submitted-revisions/submitted-revisions.component';
import {AcceptedScientificWorksComponent} from './components/scientific-works/accepted-scientific-works/accepted-scientific-works.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: 'choosing-journal',
    component: ChoosingJournalComponent
  },
  {
    path: 'journal-registration',
    component: JournalRegistrationComponent
  },
  {
    path: 'scientific-work-submission',
    component: SubmitScientificWorkComponent
  },
  {
    path: 'scientific-work-submission/:option/:id',
    component: SubmitScientificWorkComponent
  },
  {
    path: 'author-homepage',
    component: AuthorHomepageComponent
  },
  {
    path: 'reviewer-homepage',
    component: ReviewerHomepageComponent
  },
  {
    path: 'editor-homepage',
    component: EditorHomepageComponent
  },
  {
    path: 'submitted-scientific-works',
    component: SubmittedScientificWorksComponent
  },
  {
    path: 'submitted-scientific-works/:option',
    component: SubmittedScientificWorksComponent
  },
  {
    path: 'choosing-reviewers',
    component: ChoosingReviewersComponent
  },
  {
    path: 'choosing-reviewers/:additional',
    component: ChoosingReviewersComponent
  },
  {
    path: 'revision/:id',
    component: RevisionComponent
  },
  {
    path: 'revisions/:option/:id',
    component: SubmittedRevisionsComponent
  },
  {
    path: 'accepted-scientific-works',
    component: AcceptedScientificWorksComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
