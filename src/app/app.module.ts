import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import {ToastaModule} from 'ngx-toasta';
import { NgxPaginationModule } from 'ngx-pagination';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AuthorHomepageComponent } from './components/author/author-homepage/author-homepage.component';
import { ReviewerHomepageComponent } from './components/reviewer/reviewer-homepage/reviewer-homepage.component';
import { EditorHomepageComponent } from './components/editor/editor-homepage/editor-homepage.component';
import {LoginService} from './service/login.service';
import { RegistrationComponent } from './components/registration/registration.component';
import {RegistrationService} from './service/registration.service';
import {DataService} from './service/data.service';
import {JournalSevice} from './service/journal.sevice';
import { JournalRegistrationComponent } from './components/journals/journal-registration/journal-registration.component';
import { SubmitScientificWorkComponent } from './components/scientific-works/submit-scientific-work/submit-scientific-work.component';
import {ChoosingJournalComponent} from './components/journals/choosing-journal/choosing-journal.component';
import {ScientificWorkService} from './service/scientific-work.service';
import { SubmittedScientificWorksComponent } from './components/scientific-works/submitted-scientific-works/submitted-scientific-works.component';
import { AcceptedScientificWorksComponent } from './components/scientific-works/accepted-scientific-works/accepted-scientific-works.component';
import { ChoosingReviewersComponent } from './components/reviewer/choosing-reviewers/choosing-reviewers.component';
import {ReviewerService} from './service/reviewer.service';
import { RevisionComponent } from './components/reviewer/revision/revision.component';
import { SubmittedRevisionsComponent } from './components/reviewer/submitted-revisions/submitted-revisions.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AuthorHomepageComponent,
    ReviewerHomepageComponent,
    EditorHomepageComponent,
    RegistrationComponent,
    ChoosingJournalComponent,
    JournalRegistrationComponent,
    SubmitScientificWorkComponent,
    SubmittedScientificWorksComponent,
    AcceptedScientificWorksComponent,
    ChoosingReviewersComponent,
    RevisionComponent,
    SubmittedRevisionsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ToastaModule.forRoot(),
    NgxPaginationModule,
    BrowserAnimationsModule
  ],
  providers: [
    LoginService,
    RegistrationService,
    DataService,
    JournalSevice,
    ScientificWorkService,
    ReviewerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
