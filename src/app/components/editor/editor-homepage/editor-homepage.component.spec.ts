import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorHomepageComponent } from './editor-homepage.component';

describe('EditorHomepageComponent', () => {
  let component: EditorHomepageComponent;
  let fixture: ComponentFixture<EditorHomepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorHomepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorHomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
