import { Component, OnInit } from '@angular/core';
import {FormDetails} from '../../model/form-details';
import {CustomOption} from '../../model/custom-options';
import {FormSubmission} from '../../model/form-submission';
import {LoginService} from '../../service/login.service';
import {RegistrationService} from '../../service/registration.service';
import {DataService} from '../../service/data.service';
import {ToastaService} from 'ngx-toasta';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  formDetails: FormDetails = new FormDetails();
  formSubmission: FormSubmission[] = [];
  formFields: any[] = [];
  taskId: string;
  processInstanceId: string;
  flag: boolean;
  show: boolean;

  constructor(private loginService: LoginService,
              private registrationService: RegistrationService,
              private toastr: ToastaService,
              private router: Router,
              private dataService: DataService) { }

  ngOnInit() {
    this.dataService.currentFormDetails.subscribe(message => this.formDetails = message);
    this.flag = false;
    this.show = false;
    this.getForm();
  }

  getForm() {
    this.loginService.getLoginOrRegistrationForm(this.flag).then(
      res => {
        this.formFields = res.formFields;
        this.taskId = res.taskId;
        this.processInstanceId = res.processInstanceId;
        this.show = true;
      }
    ).catch(
      error => {
        console.log(error);
      }
    );
  }

  register(values) {
    let uname = '';
    for (const property in values) {
      if (property === 'username') {
        uname = values[property];
      }
      console.log('field id: ' + property + 'field value: ' + values[property]);
      this.formSubmission.push(new FormSubmission(property, values[property]));
    }
    this.registrationService.register(this.taskId, this.formSubmission).then(
      res => {
        const toastrOpt: CustomOption = new CustomOption('Valid registration', 'Registration successful!');

        this.toastr.success(toastrOpt);
        localStorage.setItem('currentUser', JSON.stringify({
          username: uname
        }));
        this.formDetails = res;
        this.dataService.changeFormDetails(this.formDetails);
        this.router.navigateByUrl('choosing-journal');
      }
    ).catch(
      error => {
        this.showErrorMessage();
      }
    );
  }

  showErrorMessage() {
    const toastrOpt: CustomOption = new CustomOption('Invalid login', 'Username or password is incorrect!');

    this.toastr.error(toastrOpt);
  }
}
