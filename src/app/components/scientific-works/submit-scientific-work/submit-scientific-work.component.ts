import {Component, OnInit, ViewChild} from '@angular/core';
import {ScientificWorkService} from '../../../service/scientific-work.service';
import {DataService} from '../../../service/data.service';
import {FormSubmission} from '../../../model/form-submission';
import {FormDetails} from '../../../model/form-details';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastaService} from 'ngx-toasta';
import {CustomOption} from '../../../model/custom-options';
import {Author} from '../../../model/author';
import {LoginService} from '../../../service/login.service';
import {ScientificWorks} from '../../../model/scientific-works';

@Component({
  selector: 'app-submit-scientific-work',
  templateUrl: './submit-scientific-work.component.html',
  styleUrls: ['./submit-scientific-work.component.css']
})
export class SubmitScientificWorkComponent implements OnInit {

  formFields: any = [];
  formFieldsRevision: any = [];
  formSubmission: FormSubmission[] = [];
  taskId: string;
  processInstanceId: string;
  enumAreas: string[] = [];
  show: boolean;
  formDetails: FormDetails = new FormDetails();
  coAuthors: Author[] = [];
  newAuthor: Author = new Author();
  changingFormat: boolean;
  changingFromRevision: boolean;
  id: number;
  scientificWork: ScientificWorks = new ScientificWorks();
  user: string;
  comment: string;
  revisionComments: string[] = [];
  @ViewChild('files') fileInput;

  constructor( private dataService: DataService,
               private scientificWorkService: ScientificWorkService,
               private router: Router,
               private toastr: ToastaService,
               private route: ActivatedRoute,
               private loginService: LoginService) { }

  ngOnInit() {
    this.show = false;
    this.changingFormat = false;
    this.changingFromRevision = false;
    if (this.route.snapshot.params['option'] && this.route.snapshot.params['id']) {
      this.id = +this.route.snapshot.params['id'];
      this.user = this.loginService.getCurrentUserUsername();
      if (this.route.snapshot.params['option'] === 'format') {
        this.getScientificWorkById('format');
        this.getComment();
      } else if (this.route.snapshot.params['option'] === 'revision') {
        this.getScientificWorkById('revision');
        this.getRevisionChangesFormDetails();
        this.getRevisionComments();
      }
    }
    this.getForm();
  }

  getForm() {
    this.dataService.currentFormDetails.subscribe(
      res => {
        this.formFields = res.formFields;
        this.taskId = res.taskId;
        this.processInstanceId = res.processInstanceId;
        this.formFields.forEach( (field) => {

          if ( field.type.name === 'enum' && field.id === 'subjectArea') {
            this.enumAreas = Object.keys(field.type.values);
          }

        });
        if (this.formFields.length !== 0) {
          this.show = true;
        }
      });
  }

  getScientificWorkById(status: string) {
    this.scientificWorkService.getScientificWorkById(this.id, this.user).then(
      res => {
        this.scientificWork = res;
        if (status === 'format') {
          this.changingFormat = true;
        } else {
          this.changingFromRevision = true;
        }
        this.show = true;
      }
    );
  }

  getComment() {
    this.scientificWorkService.getFormatInvalidComment(this.id).then(
      res => {
        this.comment = res;
      }
    );
  }

  getRevisionComments() {
    this.scientificWorkService.getRevisionComments(this.id).then(
      res => {
        this.revisionComments = res;
      }
    );
  }


  getRevisionChangesFormDetails() {
    this.scientificWorkService.getAuthorChangesWrokFormDetails(this.user).then(
      res => {
        this.formFieldsRevision = res.formFields;
      }
    );
  }

  submit(values) {

    let authors = '';
    for ( const subj of this.coAuthors ) {
      authors += subj.firstName + '-' + subj.lastName + '-' + subj.email + '-' + subj.city + '-' + subj.state + '|';
    }
    console.log(authors);

    const formData = new FormData();

    const fi = this.fileInput.nativeElement;
    if (fi.files && fi.files[0]) {
      const fileToUpload = fi.files[0];
      console.log(fileToUpload);
      formData.append('file', fileToUpload);
    }
    let comm = '';

    for (const property in values) {
      if (property !== 'pdf' && property !== 'answeringComments') {
        console.log(property + values[property]);
        formData.append(property, values[property]);
      }
      if (property === 'answeringComments') {
        comm = values[property];
      }
    }
    if (!this.changingFromRevision) {
      formData.append('authors', authors);
    }
    if (this.changingFormat || this.changingFromRevision) {
      formData.append('id', this.id.toString());
      if (this.changingFromRevision) {
        formData.append('answeringComments', comm);
      }
    }

    if (!this.changingFromRevision) {
      this.scientificWorkService.submitScientificWork(this.taskId, formData).then(
        res => {
          this.formDetails = res;
          console.log(this.formDetails);
          this.dataService.changeFormDetails(this.formDetails);
          this.showSuccessMessage();
          this.router.navigateByUrl('author-homepage');
        }
      );
    } else {
      this.scientificWorkService.submitAuthorsChangesFromRevision(this.taskId, formData).then(
        res => {
          console.log(res);
          this.showSuccessMessage();
          this.router.navigateByUrl('author-homepage');
        }
      );
    }
  }

  addNewAuthor() {
    this.coAuthors.push(this.newAuthor);
    this.newAuthor = new Author();
  }

  remove(author: Author) {
    const index: number = this.coAuthors.indexOf(author);
    if (index !== -1) {
      this.coAuthors.splice(index, 1);
    }
  }

  showSuccessMessage() {
    const toastrOpt: CustomOption = new CustomOption('Predaja rada uspješna', 'Vaš naučni rad je poslat na validaciju kod urednika!');

    this.toastr.success(toastrOpt);
  }

  getUsername(): string {
    return this.loginService.getCurrentUserUsername();
  }

}
