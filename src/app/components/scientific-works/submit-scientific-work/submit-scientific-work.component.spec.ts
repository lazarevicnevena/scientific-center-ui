import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitScientificWorkComponent } from './submit-scientific-work.component';

describe('SubmitScientificWorkComponent', () => {
  let component: SubmitScientificWorkComponent;
  let fixture: ComponentFixture<SubmitScientificWorkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitScientificWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitScientificWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
