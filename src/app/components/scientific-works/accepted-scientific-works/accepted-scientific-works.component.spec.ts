import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptedScientificWorksComponent } from './accepted-scientific-works.component';

describe('AcceptedScientificWorksComponent', () => {
  let component: AcceptedScientificWorksComponent;
  let fixture: ComponentFixture<AcceptedScientificWorksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptedScientificWorksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptedScientificWorksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
