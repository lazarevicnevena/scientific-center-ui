import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../service/login.service';
import {ScientificWorkService} from '../../../service/scientific-work.service';
import {ScientificWorks} from '../../../model/scientific-works';
import {ToastaService} from 'ngx-toasta';
import {CustomOption} from '../../../model/custom-options';
import {SearchValue} from '../../../model/search-value';
import {SearchResult} from '../../../model/search-result';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-accepted-scientific-works',
  templateUrl: './accepted-scientific-works.component.html',
  styleUrls: ['./accepted-scientific-works.component.css']
})
export class AcceptedScientificWorksComponent implements OnInit {

  show: boolean;
  works: ScientificWorks[] = [];
  pageNum: number;
  pageSize: number;
  currentPageNum: number;
  totalWorks: number;
  searchValue: SearchValue;
  searchQueries: SearchValue[] = [];
  searchResults: SearchResult[] = [];

  constructor( private loginService: LoginService,
               private scientificWorkService: ScientificWorkService,
               private toastr: ToastaService,
               public sanitizer: DomSanitizer) {
    this.pageNum = 1;
    this.pageSize = 5;
    this.currentPageNum = 1;
    this.totalWorks = 0;
    this.searchValue = new SearchValue();
  }

  ngOnInit() {
    this.show = true;
  }

  addQuery() {
    this.searchQueries.push(this.searchValue);
    this.searchValue = new SearchValue();
  }

  removeQuery(query: SearchValue) {
    const index: number = this.searchQueries.indexOf(query);
    if (index !== -1) {
      this.searchQueries.splice(index, 1);
    }
  }

  search() {
    this.scientificWorkService.search(this.searchQueries).then(
      res => {
        this.searchResults = res;
      }
    ).catch(
      error => {
        this.searchResults = [];
        this.showErrorMessage();
      }
    );
  }

  openFile(location: string) {
    console.log(location);
    const tokens = location.split('%5C');
    const email = tokens[tokens.length - 2];
    const title = tokens[tokens.length - 1];
    // web server URL
    let path = 'http://127.0.0.1:8887/searching-gateway/target/classes/files/' + email + '/' + title;
    path = decodeURIComponent(path);
    console.log(path);
    path = path.split('+').join(' ');
    window.open(path);
  }

  proceedToPaying(work: SearchResult) {
    console.log('Call paying function');
  }

  showErrorMessage() {
    const toastrOpt: CustomOption = new CustomOption('Nema zadovoljavajućih radova', 'Nijedan rad ne zadovoljava kriterijume!');

    this.toastr.error(toastrOpt);
  }
}
