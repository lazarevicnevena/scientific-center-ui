import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittedScientificWorksComponent } from './submitted-scientific-works.component';

describe('SubmittedScientificWorksComponent', () => {
  let component: SubmittedScientificWorksComponent;
  let fixture: ComponentFixture<SubmittedScientificWorksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmittedScientificWorksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedScientificWorksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
