import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../service/login.service';
import {ScientificWorkService} from '../../../service/scientific-work.service';
import {TaskDto} from '../../../model/task';
import {ScientificWorks} from '../../../model/scientific-works';
import {ToastaService} from 'ngx-toasta';
import {CustomOption} from '../../../model/custom-options';
import {FormSubmission} from '../../../model/form-submission';
import {FormDetails} from '../../../model/form-details';
import {ActivatedRoute, Router} from '@angular/router';
import {DataService} from '../../../service/data.service';
import {ReviewerService} from '../../../service/reviewer.service';
import {Reviewer} from '../../../model/reviewer';

@Component({
  selector: 'app-submitted-scientific-works',
  templateUrl: './submitted-scientific-works.component.html',
  styleUrls: ['./submitted-scientific-works.component.css']
})
export class SubmittedScientificWorksComponent implements OnInit {

  user: string;
  tasks: TaskDto[];
  works: ScientificWorks[] = [];
  taskId: string;
  processInstanceId: string;
  show: boolean;
  formSubmissions: FormSubmission[] = [];
  formDetails: FormDetails = new FormDetails();
  submittedWorks: boolean;
  reviewedWorks: boolean;
  assignedWorks: boolean;
  authorsWorks: boolean;
  authorsReviewedWorks: boolean;
  reviewerWorks: boolean;
  display = 'none';
  comment: string;
  chosenWork: ScientificWorks = new ScientificWorks();
  deadline = 10;

  constructor( private loginService: LoginService,
               private scientificWorkService: ScientificWorkService,
               private reviewerService: ReviewerService,
               private toastr: ToastaService,
               private route: ActivatedRoute,
               private router: Router,
               private dataService: DataService
  ) { }

  ngOnInit() {
    this.comment = '';
    if (this.route.snapshot.params['option']) {
      if (this.route.snapshot.params['option'] === 'assigned') {
        this.assignedWorks = true;
      } else if (this.route.snapshot.params['option'] === 'format') {
        this.authorsWorks = true;
      }  else if (this.route.snapshot.params['option'] === 'revision') {
        this.authorsReviewedWorks = true;
      } else if (this.route.snapshot.params['option'] === 'reviewers') {
        this.reviewerWorks = true;
      } else if (this.route.snapshot.params['option'] === 'reviewed') {
        this.reviewedWorks = true;
      }
    } else {
      this.submittedWorks = true;
    }

    this.show = false;
    this.user = this.loginService.getCurrentUserUsername();
    if (this.assignedWorks) {
      this.getAssignedWorks();
    } else if (this.authorsWorks) {
      this.getAuthorsWorks();
    } else if (this.authorsReviewedWorks) {
      this.getAuthorsWorksFromRevision();
    } else if (this.submittedWorks) {
      this.getSubmittedWorks();
      this.getMinorMajorChangedWorks();
    } else if (this.reviewerWorks) {
      this.getReviewerWorks();
    } else if (this.reviewedWorks) {
      this.getReviewedWorks();
    }
  }

  getSubmittedWorks() {
    this.scientificWorkService.getSubmitScientificWork(this.user).then(
      res => {
        this.tasks = res;
        for (const t of this.tasks) {
          console.log(t.taskId);
          this.works.push(t.data);
        }
      }
    );
  }

  getMinorMajorChangedWorks() {
    this.scientificWorkService.getMinorMajorChangedWorks(this.user).then(
      res => {
        for (const t of res) {
          this.tasks.push(t);
          console.log(t.taskId);
          this.works.push(t.data);
        }
        this.show = true;
      }
    );
  }

  getReviewedWorks() {
    this.scientificWorkService.getReviewedScientificWork(this.user).then(
      res => {
        this.tasks = res;
        for (const t of this.tasks) {
          console.log(t.taskId);
          this.works.push(t.data);
        }
        this.show = true;
      }
    ).catch(
      error => {
        console.log('Error: ' + error);
        this.show = false;
        this.showErrorMessage();
      }
    );
  }

  getAssignedWorks() {
    this.scientificWorkService.getAssignedWorkForRevision(this.user).then(
      res => {
        this.tasks = res;
        for (const t of this.tasks) {
          console.log(t.taskId);
          this.works.push(t.data);
        }
        this.show = true;
      }
    ).catch(
      error => {
        console.log('Error: ' + error);
        this.show = false;
        this.showErrorMessage();
      }
    );
  }

  getAuthorsWorks() {
    this.scientificWorkService.getAuthorsWorksForChanging(this.user).then(
      res => {
        this.tasks = res;
        for (const t of this.tasks) {
          console.log(t.taskId);
          this.works.push(t.data);
        }
        this.show = true;
      }
    ).catch(
      error => {
        console.log('Error: ' + error);
        this.show = false;
        this.showErrorMessage();
      }
    );
  }

  getAuthorsWorksFromRevision() {
    this.scientificWorkService.getAuthorsWorksFromRevision(this.user).then(
      res => {
        this.tasks = res;
        for (const t of this.tasks) {
          console.log(t.taskId);
          this.works.push(t.data);
        }
        this.show = true;
      }
    ).catch(
      error => {
        console.log('Error: ' + error);
        this.show = false;
        this.showErrorMessage();
      }
    );
  }

  getReviewerWorks() {
    this.scientificWorkService.getWorksForReviewing(this.user).then(
      res => {
        this.tasks = res;
        for (const t of this.tasks) {
          console.log(t.taskId);
          this.works.push(t.data);
        }
        this.show = true;
      }
    ).catch(
      error => {
        console.log('Error: ' + error);
        this.show = false;
        this.showErrorMessage();
      }
    );
  }

  validation(work: ScientificWorks, topicAcceptable: string, formatAcceptable: string) {
    this.formSubmissions = [];
    for (const t of this.tasks) {
      if (t.data.title === work.title && t.data.keywords === work.keywords) {
        this.taskId = t.taskId;
        this.processInstanceId = t.processInstanceId;
        break;
      }
    }
    const formSubmission = new FormSubmission('topicAcceptable', topicAcceptable);
    this.formSubmissions.push(formSubmission);
    const formSubmission1 = new FormSubmission('formatAcceptable', formatAcceptable);
    this.formSubmissions.push(formSubmission1);
    const formSubmission2 = new FormSubmission('swId', work.id.toString());
    this.formSubmissions.push(formSubmission2);
    if (topicAcceptable === 'true' && formatAcceptable === 'false') {
      console.log('comment: ' + this.comment + ' deadline: ' + this.deadline.toString());
      const formSubmission3 = new FormSubmission('comment', this.comment);
      this.formSubmissions.push(formSubmission3);
      const formSubmission4 = new FormSubmission('deadlineDays', this.deadline.toString());
      this.formSubmissions.push(formSubmission4);
      this.display = 'none';
    }
    this.scientificWorkService.chooseOptionForSubmittedWork(this.taskId, this.processInstanceId, this.formSubmissions).then(
      res => {
        console.log(res);
        const index: number = this.works.indexOf(work);
        if (index !== -1) {
          this.works.splice(index, 1);
        }
      }
    ).catch(
      error => {
        console.log('Error: ' + error);
      }
    );
  }

  chooseReviewers(work: ScientificWorks) {
    this.changeData(work);
    this.router.navigateByUrl('choosing-reviewers');
  }

  review(work: ScientificWorks) {
    this.changeData(work);
    const url = 'revision/' + work.id.toString();
    this.router.navigateByUrl(url);
  }

  changeScientificWork(work: ScientificWorks) {
    this.changeData(work);
    let url = '';
    if (this.authorsWorks) {
       url = 'scientific-work-submission/format/' + work.id.toString();
    } else if (this.authorsReviewedWorks) {
      url = 'scientific-work-submission/revision/' + work.id.toString();
    }
    console.log('url: ' + url);
    this.router.navigateByUrl(url);
  }

  seeRevisions(work: ScientificWorks) {
    this.changeData(work);
    const url = 'revisions/regular/' + work.id.toString();
    console.log('url:' + url);
    this.router.navigateByUrl(url);
  }

  seeRevisionsAfterChange(work: ScientificWorks) {
    this.changeData(work);
    const url = 'revisions/changes/' + work.id.toString();
    console.log('url: ' + url);
    this.router.navigateByUrl(url);
  }

  openFile(location: string) {
    const tokens = location.split('\\');
    const author = tokens[tokens.length - 2];
    const title = tokens[tokens.length - 1];
    // web server URL
    const path = 'http://127.0.0.1:8887/scientific-center-api/target/classes/files/' + author + '/' + title;
    console.log(path);
    window.open(path);
  }

  onModalClose() {
    this.display = 'none';
  }

  onModalOpen(work: ScientificWorks) {
    this.comment = '';
    this.deadline = 10;
    this.chosenWork = work;
    this.display = 'block';
  }

  showErrorMessage() {
    const toastrOpt: CustomOption = new CustomOption('Nema pristiglih radova', 'Trenutno nema pristiglih radova koje trebate pregledati!');

    this.toastr.error(toastrOpt);
  }

  sendToReviewers(work: ScientificWorks) {
    for (const t of this.tasks) {
      if (t.data.title === work.title && t.data.keywords === work.keywords) {
        this.taskId = t.taskId;
        this.processInstanceId = t.processInstanceId;
        break;
      }
    }
    const reviewers: Reviewer[] = [];
    reviewers.push(new Reviewer());
    this.reviewerService.setReviewers(this.processInstanceId, this.taskId, reviewers, this.deadline, 'second').then(
      res => {
        console.log(res);
        this.router.navigateByUrl('editor-homepage');
      }
    );
  }

  changeData(work: ScientificWorks) {
    for (const t of this.tasks) {
      if (t.data.title === work.title && t.data.keywords === work.keywords) {
        this.taskId = t.taskId;
        this.processInstanceId = t.processInstanceId;
        break;
      }
    }
    this.formDetails.taskId = this.taskId;
    this.formDetails.processInstanceId = this.processInstanceId;
    this.dataService.changeFormDetails(this.formDetails);
  }
}
