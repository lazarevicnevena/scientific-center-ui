import { Component, OnInit } from '@angular/core';
import {ToastaService} from 'ngx-toasta';
import {LoginService} from '../../service/login.service';
import { Router } from '@angular/router';
import {CustomOption} from '../../model/custom-options';
import {FormDetails} from '../../model/form-details';
import {FormSubmission} from '../../model/form-submission';
import {DataService} from '../../service/data.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formDetails: FormDetails = new FormDetails();
  formSubmission: FormSubmission[] = [];
  formFields: any[] = [];
  taskId: string;
  processInstanceId: string;
  flag: boolean;
  show: boolean;
  role: string;

  constructor(private loginService: LoginService,
              private router: Router,
              private toastr: ToastaService,
              private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.currentFormDetails.subscribe(message => this.formDetails = message);
    this.flag = true;
    this.show = false;
    this.role = '';
    this.getForm();
  }

  getForm() {
    this.loginService.getLoginOrRegistrationForm(this.flag).then(
      res => {
        this.formFields = res.formFields;
        this.taskId = res.taskId;
        this.processInstanceId = res.processInstanceId;
        this.show = true;
      }
    ).catch(
      error => {
        console.log(error);
      }
    );
  }

  submit(values) {
    let uname = '';
    for (const property in values) {
      if (property === 'username') {
        uname = values[property];
      }
      this.formSubmission.push(new FormSubmission(property, values[property]));
    }
    this.loginService.login(this.taskId, this.formSubmission).then(
      res => {
        if (res.formFields.length === 1) {
          const toastrOpt: CustomOption = new CustomOption('Logovanje uspješno', 'Korisničko ime i lozinka uspješne!');
          this.toastr.success(toastrOpt);
          localStorage.setItem('currentUser', JSON.stringify({
            username: uname
          }));
          this.formDetails = res;
          console.log(this.formDetails);
          this.loginService.getRole(uname).then(
            role => {
              console.log('Role: ' + role);
              this.role = role;
              if (this.role === 'editor') {
                this.changeCurrentUser(role);
                this.router.navigateByUrl('editor-homepage');
              } else {
                this.dataService.changeFormDetails(this.formDetails);
                this.router.navigateByUrl('choosing-journal');
              }
            }
          );
        } else {
          this.showErrorMessage();
        }

      }
    ).catch(
      error => {
        this.showErrorMessage();
      }
    );
  }

  showErrorMessage() {
    const toastrOpt: CustomOption = new CustomOption('Neuspješno logovanje', 'Korisničko ime ili lozinka su neispravni!');

    this.toastr.error(toastrOpt);
  }

  changeCurrentUser(role: string) {
    const uname = this.loginService.getCurrentUserUsername();
    localStorage.setItem('currentUser', JSON.stringify({
      username: uname,
      role: role
    }));
  }

}
