import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosingJournalComponent } from './choosing-journal.component';

describe('ChoosingJournalComponent', () => {
  let component: ChoosingJournalComponent;
  let fixture: ComponentFixture<ChoosingJournalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoosingJournalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoosingJournalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
