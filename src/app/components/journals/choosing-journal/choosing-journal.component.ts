import { Component, OnInit } from '@angular/core';
import {DataService} from '../../../service/data.service';
import {Journal} from '../../../model/journal';
import {JournalSevice} from '../../../service/journal.sevice';
import {FormSubmission} from '../../../model/form-submission';
import {FormDetails} from '../../../model/form-details';
import { Router } from '@angular/router';
import {LoginService} from '../../../service/login.service';

@Component({
  selector: 'app-choosing-journal',
  templateUrl: './choosing-journal.component.html',
  styleUrls: ['./choosing-journal.component.css']
})
export class ChoosingJournalComponent implements OnInit {

  formDetails: FormDetails = new FormDetails();
  formFields: any = [];
  formSubmission: FormSubmission[] = [];
  taskId: string;
  processInstanceId: string;
  show: boolean;
  journals: Journal[];
  pageNum: number;
  pageSize: number;
  currentPageNum: number;
  totalJournals: number;
  areas: string;
  role: string;

  constructor(private dataService: DataService,
              private journalService: JournalSevice,
              private router: Router,
              private loginService: LoginService) {
    this.pageNum = 1;
    this.pageSize = 5;
    this.currentPageNum = 1;
    this.totalJournals = 0;
    this.areas = '';
    this.role = '';
  }

  ngOnInit() {
    this.getJournals();
    this.getForm();
  }

  getJournals() {
    this.journalService.getJournals(this.pageNum - 1, this.pageSize).then(
      res => {
        this.journals = res.body as Journal[];
        this.totalJournals = +res.headers.get('total-count');
      }
    );
  }

  pageChanged($event) {
    this.currentPageNum = $event;
    this.pageNum = this.currentPageNum;
    this.journalService.getJournals(this.pageNum - 1, this.pageSize).then(
      data => {
        this.journals = data.body as Journal[];
        this.totalJournals = +data.headers.get('total-count');
      }, error => {
        console.log(error);
      }
    );
  }

  chooseJournal(issn: string) {
    this.formSubmission.push(new FormSubmission(this.formFields[0].id, issn));
    console.log('Odabran casopis: ' + issn);
    this.journalService.chooseJournal(this.taskId, this.formSubmission).then(
      res => {
        this.formDetails = res;
        console.log(this.formDetails);
        this.dataService.changeFormDetails(this.formDetails);
        if (res.formFields.length === 8) {
          this.router.navigateByUrl('journal-registration');
        } else if (res.formFields.length === 6) {
          const uname = this.getUsername();
          this.loginService.getRole(uname).then(
            role => {
              this.role = role;
              this.changeCurrentUser(role);
              if (this.role === 'author') {
                this.router.navigateByUrl('author-homepage');
              } else if (this.role === 'reviewer') {
                this.router.navigateByUrl('reviewer-homepage');
              } else if (this.role === 'editor') {
                this.router.navigateByUrl('editor-homepage');
              }
            }
          );
        } else if (res.formFields.length === 2) {
          this.router.navigateByUrl('login');
        }
      }
    );
  }

  getForm() {
    this.dataService.currentFormDetails.subscribe(
      res => {
        this.formFields = res.formFields;
        this.taskId = res.taskId;
        this.processInstanceId = res.processInstanceId;
      });
    console.log(this.formFields);
    this.show = true;
  }

  changeCurrentUser(role: string) {
    const uname = this.getUsername();
    localStorage.setItem('currentUser', JSON.stringify({
      username: uname,
      role: role
    }));
  }

  getUsername(): string {
    return this.loginService.getCurrentUserUsername();
  }

  getRole(): string {
    return this.loginService.getCurrentUserRole();
  }
}
