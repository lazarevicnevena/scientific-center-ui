import { Component, OnInit } from '@angular/core';
import {DataService} from '../../../service/data.service';
import {JournalSevice} from '../../../service/journal.sevice';
import {FormSubmission} from '../../../model/form-submission';
import {FormDetails} from '../../../model/form-details';
import { Router } from '@angular/router';
import {LoginService} from '../../../service/login.service';

@Component({
  selector: 'app-journal-registration',
  templateUrl: './journal-registration.component.html',
  styleUrls: ['./journal-registration.component.css']
})
export class JournalRegistrationComponent implements OnInit {

  formFields: any = [];
  formSubmission: FormSubmission[] = [];
  taskId: string;
  processInstanceId: string;
  enumAreas: string[] = [];
  enumRole: string[] = [];
  show: boolean;
  role: string;
  chosenSubjects: string[] = [];
  chosenSubject: string;
  formDetails: FormDetails = new FormDetails();

  constructor( private dataService: DataService,
               private journalService: JournalSevice,
               private router: Router,
               private loginService: LoginService) { }

  ngOnInit() {
    this.show = false;
    this.role = 'author';
    this.chosenSubject = 'Astronomija';
    this.getForm();
  }

  getForm() {
    this.dataService.currentFormDetails.subscribe(
      res => {
        this.formFields = res.formFields;
        this.taskId = res.taskId;
        this.processInstanceId = res.processInstanceId;
        this.formFields.forEach( (field) => {

          if ( field.type.name === 'enum' && field.id === 'subjectArea') {
            this.enumAreas = Object.keys(field.type.values);
          }

          if ( field.type.name === 'enum' && field.id === 'role') {
            this.enumRole = Object.keys(field.type.values);
          }
        });
        this.show = true;
      });
    console.log(this.formFields);

  }

  choose() {
    const index: number = this.chosenSubjects.indexOf(this.chosenSubject);
    if (index === -1) {
      this.chosenSubjects.push(this.chosenSubject);
    }
  }

  remove(subject: string) {
    const index: number = this.chosenSubjects.indexOf(subject);
    if (index !== -1) {
      this.chosenSubjects.splice(index, 1);
    }
  }

  register(values) {
    let areas = '';
    for (const subj of this.chosenSubjects) {
      areas += subj + '|';
    }
    areas = areas.substring(0, areas.length - 1);
    console.log(areas);
    for (const property in values) {
      if (property !== 'subjectArea') {
        this.formSubmission.push(new FormSubmission(property, values[property]));
      }
    }
    this.formSubmission.push(new FormSubmission('subjectArea', areas));
    console.log(this.formSubmission);
    this.journalService.registrationForJournal(this.taskId, this.formSubmission).then(
      res => {
        this.formDetails = res;
        console.log(this.formDetails);
        this.dataService.changeFormDetails(this.formDetails);
        this.changeCurrentUser();
        if (this.role === 'author') {
          this.router.navigateByUrl('author-homepage');
        } else if (this.role === 'reviewer') {
          this.router.navigateByUrl('reviewer-homepage');
        } else if (this.role === 'editor') {
          this.router.navigateByUrl('editor-homepage');
        }
      }
    );
  }

  getUsername(): string {
    return this.loginService.getCurrentUserUsername();
  }

  changeCurrentUser() {
    const uname = this.getUsername();
    localStorage.setItem('currentUser', JSON.stringify({
      username: uname,
      role: this.role
    }));
  }


}
