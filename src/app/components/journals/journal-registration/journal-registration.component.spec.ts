import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalRegistrationComponent } from './journal-registration.component';

describe('JournalRegistrationComponent', () => {
  let component: JournalRegistrationComponent;
  let fixture: ComponentFixture<JournalRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JournalRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
