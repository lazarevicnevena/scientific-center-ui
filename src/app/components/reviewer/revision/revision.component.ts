import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../service/login.service';
import {DataService} from '../../../service/data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ReviewerService} from '../../../service/reviewer.service';
import {FormSubmission} from '../../../model/form-submission';
import {CustomOption} from '../../../model/custom-options';
import {ToastaService} from 'ngx-toasta';
import {MinorChangesDetails} from '../../../model/minor-changes-details';
import {ScientificWorkService} from '../../../service/scientific-work.service';

@Component({
  selector: 'app-revision',
  templateUrl: './revision.component.html',
  styleUrls: ['./revision.component.css']
})
export class RevisionComponent implements OnInit {

  taskId: string;
  processInstanceId: string;
  show: boolean;
  formFields: any = [];
  formSubmission: FormSubmission[] = [];
  enumRecommendation: string[] = [];
  reviewer: string;
  chosenRecommendation = 'Prihvatiti';
  id: number;
  majorChangesDetails: MinorChangesDetails;
  majorShow: boolean;

  constructor( private loginService: LoginService,
               private dataService: DataService,
               private router: Router,
               private route: ActivatedRoute,
               private reviewerService: ReviewerService,
               private scientificWorkService: ScientificWorkService,
               private toastr: ToastaService) { }

  ngOnInit() {
    this.show = false;
    this.majorShow = false;
    if (this.route.snapshot.params['id']) {
      this.id = +this.route.snapshot.params['id'];
      this.reviewer = this.loginService.getCurrentUserUsername();
      this.getTaskIdAnsProccInsId();
      this.getFormFields();
      this.getMajorChangeDetails();
    }
  }

  getTaskIdAnsProccInsId() {
    this.dataService.currentFormDetails.subscribe(
      res => {
        this.taskId = res.taskId;
        this.processInstanceId = res.processInstanceId;
      });

  }
  getFormFields() {
    this.reviewerService.getRevisionFormDetails(this.reviewer).then(
      res => {
        this.formFields = res.formFields;
        this.formFields.forEach( (field) => {

          if ( field.type.name === 'enum' && field.id === 'recommendation') {
            this.enumRecommendation = Object.keys(field.type.values);
          }

        });
        if (this.formFields.length !== 0) {
          this.show = true;
        }
      }
    );
  }

  sendRevision(values) {

    for (const property in values) {
      console.log(property + values[property]);
      const formSubmission1 = new FormSubmission(property, values[property]);
      this.formSubmission.push(formSubmission1);
    }

    this.reviewerService.sendRevision(this.reviewer, this.taskId, this.processInstanceId, this.formSubmission).then(
      res => {
        console.log(res);
        this.showSuccessMessage();
        this.router.navigateByUrl('reviewer-homepage');
      }
    );
  }

  getMajorChangeDetails() {
    this.scientificWorkService.getMinorChangesDetails(this.id).then(
      res => {
        this.majorChangesDetails = res;
        this.majorShow = true;
      }
    ).catch(
      res => {
        this.majorShow = false;
      }
    );
  }

  openFile(location: string) {
    const tokens = location.split('\\');
    const author = tokens[tokens.length - 2];
    const title = tokens[tokens.length - 1];
    // web server URL
    const path = 'http://127.0.0.1:8887/scientific-center-api/target/classes/files/' + author + '/' + title;
    console.log(path);
    window.open(path);
  }

  showSuccessMessage() {
    const toastrOpt: CustomOption = new CustomOption('Predaja revizije uspješna', 'Vaša revizija je poslta uredniku!');

    this.toastr.success(toastrOpt);
  }

}
