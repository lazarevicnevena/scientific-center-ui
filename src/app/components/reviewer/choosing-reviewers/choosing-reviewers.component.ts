import { Component, OnInit } from '@angular/core';
import {DataService} from '../../../service/data.service';
import {LoginService} from '../../../service/login.service';
import {ToastaService} from 'ngx-toasta';
import {CustomOption} from '../../../model/custom-options';
import {ReviewerService} from '../../../service/reviewer.service';
import {Reviewer} from '../../../model/reviewer';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-choosing-reviewers',
  templateUrl: './choosing-reviewers.component.html',
  styleUrls: ['./choosing-reviewers.component.css']
})
export class ChoosingReviewersComponent implements OnInit {

  option: string;
  taskId: string;
  processInstanceId: string;
  reviewers: Reviewer[] = [];
  show: boolean;
  totalNeeded: number;
  reviewersChosen: Reviewer[] = [];
  display = 'none';
  deadline = 10;
  additional: boolean;

  constructor( private loginService: LoginService,
               private toastr: ToastaService,
               private dataService: DataService,
               private reviewerService: ReviewerService,
               private router: Router,
               private route: ActivatedRoute) { }

  ngOnInit() {
    this.option = 'all';
    this.show = false;
    this.getData();
    if (this.route.snapshot.params['additional']) {
      this.additional = true;
      this.additionalRevisionNeeded();
    } else {
      this.additional = false;
    }
  }

  getReviewers() {
    console.log('Option selected is: ' + this.option);
    this.reviewerService.getReviewers(this.processInstanceId, this.option, this.additional).then(
      res => {
        this.reviewers = res.body as Reviewer[];
        this.totalNeeded = +res.headers.get('total-needed');
        this.show = true;
      }
    ).catch(
      error => {
        this.show = false;
        this.showErrorMessage();
      }
    );
  }

  getData() {
    this.dataService.currentFormDetails.subscribe(
      res => {
        this.taskId = res.taskId;
        this.processInstanceId = res.processInstanceId;
      });

  }

  addNewReviewer(reviewer: Reviewer) {
    const index: number = this.reviewersChosen.indexOf(reviewer);
    if (index === -1) {
      this.reviewersChosen.push(reviewer);
    }
  }

  removeReviewer(reviewer: Reviewer) {
    const index: number = this.reviewersChosen.indexOf(reviewer);
    if (index !== -1) {
      this.reviewersChosen.splice(index, 1);
    }
  }

  sendRevision() {
    this.reviewerService.setReviewers(this.processInstanceId, this.taskId, this.reviewersChosen, this.deadline, 'first').then(
      res => {
        console.log(res);
        this.router.navigateByUrl('editor-homepage');
      }
    );
  }

  additionalRevisionNeeded() {
    this.reviewerService.additionalRevisionNeeded(this.processInstanceId, this.taskId).then(
      res => {
        console.log(res);
        this.taskId = res.taskId;
        this.processInstanceId = res.processInstanceId;
      }
    );
  }

  onModalClose() {
    this.display = 'none';
  }

  onModalOpen() {
    this.deadline = 10;
    this.display = 'block';
  }

  showErrorMessage() {
    const toastrOpt: CustomOption = new CustomOption('Nema recenzenata', 'Ne postoje recenzenti koji zadovoljavaju tražene uslove!');

    this.toastr.error(toastrOpt);
  }

  showMessageEnough() {
    const toastrOpt: CustomOption = new CustomOption('Dovoljno recenzenata', 'Već ste izabrali dovoljan broj recenzenata!');

    this.toastr.error(toastrOpt);
  }
}
