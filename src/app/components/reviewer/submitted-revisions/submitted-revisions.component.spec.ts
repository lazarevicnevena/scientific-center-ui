import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittedRevisionsComponent } from './submitted-revisions.component';

describe('SubmittedRevisionsComponent', () => {
  let component: SubmittedRevisionsComponent;
  let fixture: ComponentFixture<SubmittedRevisionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmittedRevisionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedRevisionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
