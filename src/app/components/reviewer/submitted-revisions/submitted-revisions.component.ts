import { Component, OnInit } from '@angular/core';
import {ScientificWorks} from '../../../model/scientific-works';
import {DataService} from '../../../service/data.service';
import {ReviewerService} from '../../../service/reviewer.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastaService} from 'ngx-toasta';
import {ScientificWorkService} from '../../../service/scientific-work.service';
import {ReviewDetails} from '../../../model/review-details';
import {LoginService} from '../../../service/login.service';
import {FormSubmission} from '../../../model/form-submission';
import {CustomOption} from '../../../model/custom-options';
import {MinorChangesDetails} from '../../../model/minor-changes-details';

@Component({
  selector: 'app-submitted-revisions',
  templateUrl: './submitted-revisions.component.html',
  styleUrls: ['./submitted-revisions.component.css']
})
export class SubmittedRevisionsComponent implements OnInit {

  id: number;
  taskId: string;
  processInstanceId: string;
  show: boolean;
  reviews: ReviewDetails[] = [];
  display = 'none';
  decision: string;
  editor: string;
  enumDecision: string[] = [];
  formFields: any = [];
  chosenDecision = 'Prihvatiti';
  formSubmission: FormSubmission[] = [];
  regularShow: boolean;
  minorChangesShow: boolean;
  isAccepted: boolean;
  hours = 24;
  minorChangesDeatils: MinorChangesDetails;

  constructor( private dataService: DataService,
               private loginService: LoginService,
               private reviewerService: ReviewerService,
               private router: Router,
               private toastr: ToastaService,
               private scientificWorkService: ScientificWorkService,
               private route: ActivatedRoute) { }

  ngOnInit() {
    this.editor = this.loginService.getCurrentUserUsername();
    this.show = false;
    this.regularShow = false;
    this.minorChangesShow = false;
    this.getTaskIdAndProccInsId();
    if (this.route.snapshot.params['id'] && this.route.snapshot.params['option']) {
      this.decision = 'Prihvatiti';
      this.id = +this.route.snapshot.params['id'];
      this.getRevisions();
      if (this.route.snapshot.params['option'] === 'regular') {
        this.getFormFields();
        this.regularShow = true;
      } else if (this.route.snapshot.params['option'] === 'changes') {
        this.isAccepted = false;
        this.minorChangesDeatils  = new MinorChangesDetails();
        this.getMinorChangeDetails();
        this.getEditorSecondFormFields();
        this.minorChangesShow = true;
      }
    }
  }

  getRevisions() {
    this.scientificWorkService.getRevisionsForSW(this.id).then(
      res => {
        this.reviews = res;
        if (this.reviews.length > 0) {
          this.show = true;
        }
      }
    );
  }

  getTaskIdAndProccInsId() {
    this.dataService.currentFormDetails.subscribe(
      res => {
        this.taskId = res.taskId;
        this.processInstanceId = res.processInstanceId;
      });
  }

  getFormFields() {
    this.reviewerService.getEditorDecisionFormDetails(this.editor).then(
      res => {
        // this.formFields = res.formFields;
        res.formFields.forEach( (field) => {
          if ( field.type.name === 'enum' && field.id === 'decision') {
            this.enumDecision = Object.keys(field.type.values);
          }
          if (field.id !== 'additionalRevision') {
            this.formFields.push(field);
          }

        });
        if (this.formFields.length !== 0) {
          this.show = true;
        }
      }
    );
  }

  getEditorSecondFormFields() {
    this.scientificWorkService.getEditorSecondDecisionFormDetails(this.editor).then(
      res => {
        this.formFields = res.formFields;
        if (this.formFields.length !== 0) {
          this.show = true;
        }
      }
    );
  }

  onModalClose() {
    this.display = 'none';
  }

  onModalOpen() {
    this.decision = 'accept';
    this.display = 'block';
  }

  additionalRevision() {
    this.router.navigateByUrl('choosing-reviewers/true');
  }

  decide(values) {

    for (const property in values) {
      console.log(property + values[property]);
      const formSubmission1 = new FormSubmission(property, values[property]);
      this.formSubmission.push(formSubmission1);
    }

    let option = '';
    if (this.regularShow) {
      option = 'first';
    } else if (this.minorChangesShow) {
      option = 'second';
    }
    this.reviewerService.decide(this.taskId, this.processInstanceId, this.formSubmission, option).then(
      res => {
        console.log(res);
        this.showSuccessMessage();
        this.router.navigateByUrl('editor-homepage');
      }
    );
  }

  getMinorChangeDetails() {
    this.scientificWorkService.getMinorChangesDetails(this.id).then(
      res => {
        this.minorChangesDeatils = res;
      }
    );
  }

  openFile(location: string) {
    const tokens = location.split('\\');
    const author = tokens[tokens.length - 2];
    const title = tokens[tokens.length - 1];
    // web server URL
    const path = 'http://127.0.0.1:8887/scientific-center-api/target/classes/files/' + author + '/' + title;
    console.log(path);
    window.open(path);
  }

  showSuccessMessage() {
    const toastrOpt: CustomOption = new CustomOption('Donijeli ste odluku o radu', 'Autor će biti obaviješten o Vašoj odluci!');

    this.toastr.success(toastrOpt);
  }
}
